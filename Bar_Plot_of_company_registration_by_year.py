import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv('Maharashtra.csv', usecols=[
                 'DATE_OF_REGISTRATION'], encoding='unicode_escape')

dict1 = {'00': 0}
for i in range(1, 100):
    dict1[str(i)] = 0

for i in range(len(df)):
    if str(df['DATE_OF_REGISTRATION'][i])[-2::] in dict1:
        dict1[str(df['DATE_OF_REGISTRATION'][i])[-2::]] += 1
    else:
        dict1[str(df['DATE_OF_REGISTRATION'][i])[-2::]] = 0

plt.bar(dict1.keys(), dict1.values())
plt.xticks(
    rotation=90,
    fontsize=5
)
plt.show()
