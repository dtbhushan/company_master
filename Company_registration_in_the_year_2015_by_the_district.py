import pandas as pd
from matplotlib import pyplot as plt

df1 = pd.read_csv('Maharashtra.csv', usecols=[
                'DATE_OF_REGISTRATION', 'Registered_Office_Address'],
                encoding='unicode_escape')
df2 = pd.read_csv('pin_code.csv')

d2 = {}
for i in range(1, len(df2)):
    d2[str(df2['Pin Code'][i])] = str(df2['District'][i])

dict1 = {}
for i in range(len(df1)):
    if str(df1['DATE_OF_REGISTRATION'][i])[-2::] == '15':
        try:
            if d2[str(df1['Registered_Office_Address'][i])[-6::]] in dict1:

                dict1[d2[str(df1['Registered_Office_Address'][i])[-6::]]] += 1
            else:
                dict1[d2[str(df1['Registered_Office_Address'][i])[-6::]]] = 1

        except Exception:
            pass

plt.bar(dict1.keys(), dict1.values())
plt.xticks(rotation=45,
           horizontalalignment='right')
plt.show()
