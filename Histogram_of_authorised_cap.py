import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv('Maharashtra.csv', usecols=[
                 'Company_Name', 'AUTHORIZED_CAP'], encoding='unicode_escape')
dict1 = {'<= 1L': 0, '1L to 10L': 0,
         '10L to 1Cr': 0, '1Cr to 10Cr': 0, '> 10Cr': 0}

for i in range(len(df)):
    if df['AUTHORIZED_CAP'][i] <= 100000:
        dict1['<= 1L'] += 1
    elif df['AUTHORIZED_CAP'][i] <= 1000000:
        dict1['1L to 10L'] += 1
    elif df['AUTHORIZED_CAP'][i] <= 10000000:
        dict1['10L to 1Cr'] += 1
    elif df['AUTHORIZED_CAP'][i] <= 100000000:
        dict1['1Cr to 10Cr'] += 1
    elif df['AUTHORIZED_CAP'][i] > 100000000:
        dict1['> 10Cr'] += 1

plt.bar(dict1.keys(), dict1.values())
plt.show()
